package com.facci.rayoalquiler;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import androidx.test.filters.LargeTest;
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

@LargeTest
@RunWith(AndroidJUnit4ClassRunner.class)
public class UnitTest {
    @Test
    public void ReturnTrue(){
        com.facci.rayoalquiler.tests.Test test = new com.facci.rayoalquiler.tests.Test();
        assertTrue(test.validNoEmpty("alex-vel", "123456"));
    }

    @Test
    public void ReturnFalse(){
        com.facci.rayoalquiler.tests.Test test = new com.facci.rayoalquiler.tests.Test();
        assertTrue(test.validEmpty("alex-vel", "123456"));
    }
    @Test
    public void validUserEmail(){
        com.facci.rayoalquiler.tests.Test test = new com.facci.rayoalquiler.tests.Test();
        assertTrue(test.validEmail("alex@gmail.com"));
    }

}
