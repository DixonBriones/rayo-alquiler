package com.facci.rayoalquiler;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.filters.LargeTest;
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner;

import com.facci.rayoalquiler.contans.Auth;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

@LargeTest
@RunWith(AndroidJUnit4ClassRunner.class)
public class RegistroAutoTestUI {
    public static final String MARCA = "Chevrolet";
    public static final String MODELO = "Aveo";

    @Rule
    public ActivityScenarioRule<AuthActivity> activityActivityScenarioRule = new ActivityScenarioRule<AuthActivity>(AuthActivity.class);

    @Rule
    public ActivityScenarioRule<MainActivity> mainActivityScenarioRule = new ActivityScenarioRule<MainActivity>(MainActivity.class);

    @Rule
    public ActivityScenarioRule<FormRegistroAuto> formActivityScenarioRule = new ActivityScenarioRule<FormRegistroAuto>(FormRegistroAuto.class);
    @Test
    public void RegisterAuto() {
        onView(withId(R.id.edtUsername)).perform(typeText("alex-vel"), closeSoftKeyboard());
        onView(withId(R.id.edtPassword)).perform(typeText("123456"), closeSoftKeyboard());
        onView(withId(R.id.btnLogin)).perform(click());
        onView(withId(R.id.autosFragment)).perform(click());
        onView(withId(R.id.fabCreateAuto)).perform(click());
        onView(withId(R.id.edtAutoBrand)).perform(typeText(MARCA), closeSoftKeyboard());
        onView(withId(R.id.edtAutoModel)).perform(typeText(MODELO), closeSoftKeyboard());
        onView(withId(R.id.btnRegisterAuto)).perform(click());
    }
}
