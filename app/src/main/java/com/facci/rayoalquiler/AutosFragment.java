package com.facci.rayoalquiler;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.facci.rayoalquiler.adapter.AutosApiAdapter;
import com.facci.rayoalquiler.adapter.MisAutosAdapter;
import com.facci.rayoalquiler.contans.Auth;
import com.facci.rayoalquiler.models.ClaseAuto;
import com.facci.rayoalquiler.models.ModelUser;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AutosFragment extends Fragment {

    ListView listView;
    MisAutosAdapter misAutosAdapter;
    private ArrayList<ModelUser> userArrayList;
    private ModelUser user;
    private Dialog dialog;

    public AutosFragment() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_autos, container, false);

        dialog = new Dialog(getContext(), R.style.appDialog);

        //LIST AUTO
        listView = view.findViewById(R.id.list_person);
        listAutosByUser();

        FloatingActionButton btnCreateAuto = view.findViewById(R.id.fabCreateAuto);
        btnCreateAuto.setOnClickListener(views -> {
            Intent i = new Intent(getActivity(), FormRegistroAuto.class);
            startActivity(i);
        });

        Log.e("LOG", Auth.TOKEN);
        return view;
    }

    public void listAutosByUser(){
        AutosApiAdapter autosApiAdapter = new AutosApiAdapter();
        Call<ArrayList<ClaseAuto>> call = autosApiAdapter.getAutoByUser(Auth.TOKEN, Auth._ID);
        call.enqueue(new Callback<ArrayList<ClaseAuto>>() {
            @Override
            public void onResponse(Call<ArrayList<ClaseAuto>> call, Response<ArrayList<ClaseAuto>> response) {
                ArrayList<ClaseAuto> arrayList = response.body();
                for(ClaseAuto auto : arrayList){
                    //Log.e("AUTOS", auto.getModelo());
                }

                misAutosAdapter = new MisAutosAdapter(getActivity(), arrayList);
                listView.setAdapter(misAutosAdapter);
            }

            @Override
            public void onFailure(Call<ArrayList<ClaseAuto>> call, Throwable t) {
                Toast.makeText(getActivity(), "Connection Fail", Toast.LENGTH_SHORT).show();
            }
        });
    }
}