package com.facci.rayoalquiler;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import java.util.Timer;
import java.util.TimerTask;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.Theme_RayoAlquiler);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        TimerTask splashScreen = new TimerTask() {
            @Override
            public void run() {
                startActivity(new Intent(SplashScreen.this, AuthActivity.class));
                finish();
            }
        };
        Timer time = new Timer();
        time.schedule(splashScreen, 100);
    }
}