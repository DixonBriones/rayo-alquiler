package com.facci.rayoalquiler;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.facci.rayoalquiler.adapter.AutosApiAdapter;
import com.facci.rayoalquiler.adapter.PrincipalAdapter;
import com.facci.rayoalquiler.contans.Auth;
import com.facci.rayoalquiler.models.ClaseAuto;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PrincipalFragment#newInstance} factory method to
 * create an instance of this fragment.
 */


public class PrincipalFragment extends Fragment {
    RecyclerView recyclerView;
    PrincipalAdapter principalAdapter;
    ArrayList<ClaseAuto> listaAutos;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public PrincipalFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PrincipalFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PrincipalFragment newInstance(String param1, String param2) {
        PrincipalFragment fragment = new PrincipalFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_principal, container, false);
        try {
            Thread.sleep(1*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        mostrarDatos(view);
        return view;

    }

    public void onDestroyView() {
        super.onDestroyView();
    }

    public void mostrarDatos(View view ) {
        AutosApiAdapter autosApiAdapter = new AutosApiAdapter();

        Call<ArrayList<ClaseAuto>> call = autosApiAdapter.getAutos(Auth.TOKEN);

        call.enqueue(new Callback<ArrayList<ClaseAuto>>() {

            @Override
            public void onResponse(Call<ArrayList<ClaseAuto>> call, Response<ArrayList<ClaseAuto>> response) {

                listaAutos= new ArrayList<>();
                listaAutos = response.body();
                recyclerView= view.findViewById(R.id.rcvPrincipal);
                principalAdapter= new PrincipalAdapter(listaAutos, getActivity());

                recyclerView.setAdapter(principalAdapter);
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL,false));
                principalAdapter.notifyDataSetChanged();

                for (ClaseAuto claseAuto : listaAutos) {
                    Log.e("Datos", claseAuto.getMarca());
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ClaseAuto>> call, Throwable t) {
                Log.e("Error", "Se encontro un error");
            }
        });

    }
}