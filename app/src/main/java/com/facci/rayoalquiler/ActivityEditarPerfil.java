package com.facci.rayoalquiler;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facci.rayoalquiler.adapter.AdapterUser;
import com.facci.rayoalquiler.adapter.AutosApiAdapter;
import com.facci.rayoalquiler.contans.Auth;
import com.facci.rayoalquiler.models.ClaseAuto;
import com.facci.rayoalquiler.models.ModelUser;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityEditarPerfil extends AppCompatActivity {

    EditText edtUsername, edtUseremail, edtTelephone, edtFirstname, edtLastname, edtLocation;
    Button btnUpdateUser, btnCancelUpdateUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_perfil);

        edtUseremail = findViewById(R.id.edtTxtUseremail);
        edtTelephone = findViewById(R.id.edtTxtTelephone);
        edtFirstname = findViewById(R.id.edtTxtFirstname);
        edtLastname = findViewById(R.id.edtTxtLastname);
        edtLocation = findViewById(R.id.edtTxtLocation);

        btnCancelUpdateUser = findViewById(R.id.btnCancelUpdateUser);
        btnCancelUpdateUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ActivityEditarPerfil.this, MainActivity.class));
            }
        });

        Log.e("ID", Auth._ID);

        btnUpdateUser = findViewById(R.id.btnUpdateUser);
        btnUpdateUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(TextUtils.isEmpty(edtTelephone.getText().toString()) && TextUtils.isEmpty(edtUseremail.getText().toString()) &&
                        TextUtils.isEmpty(edtFirstname.getText().toString()) && TextUtils.isEmpty(edtLastname.getText().toString()) && TextUtils.isEmpty(edtLocation.getText().toString())){
                    Toast.makeText(ActivityEditarPerfil.this, "Fill in the fields", Toast.LENGTH_SHORT).show();
                }else{
                    updateUser(Auth._ID,
                            new ModelUser(edtTelephone.getText().toString(),
                                    edtUseremail.getText().toString(),
                                    edtFirstname.getText().toString(),
                                    edtLastname.getText().toString(),
                                    edtLocation.getText().toString()));
                }
            }
        });
    }

    public void updateUser(String id, ModelUser modelUser){
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityEditarPerfil.this);
        builder.setTitle("Update Data User");
        builder.setMessage("¿Estas seguro que desea actualizar sus datos?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        AdapterUser adapterUser = new AdapterUser();
                        adapterUser.userUpdate(id, modelUser)
                                .enqueue(new Callback<ModelUser>() {
                                    @Override
                                    public void onResponse(Call<ModelUser> call, Response<ModelUser> response) {
                                        if (response.isSuccessful()) {
                                            Toast.makeText(ActivityEditarPerfil.this, "Data update successfully!", Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<ModelUser> call, Throwable t) {
                                        Log.e("Error", "xd of error");
                                        Toast.makeText(ActivityEditarPerfil.this, "Fail app", Toast.LENGTH_SHORT).show();
                                    }
                                });
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(getApplicationContext(), "Cancel", Toast.LENGTH_SHORT).show();
                        dialogInterface.dismiss();
                    }
                }).show();
    }
}