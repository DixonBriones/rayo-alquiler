package com.facci.rayoalquiler.adapter;

import com.facci.rayoalquiler.contans.AutosApi;
import com.facci.rayoalquiler.models.BodyAlquiler;
import com.facci.rayoalquiler.models.ClaseAuto;
import com.facci.rayoalquiler.models.ModelAlquiler;
import com.facci.rayoalquiler.models.ModelUser;
import com.facci.rayoalquiler.services.AutosService;

import java.util.ArrayList;
import java.util.Date;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;

public class AutosApiAdapter extends BaseAdapter implements AutosService {
    private AutosService autosService;

    public AutosApiAdapter(){
        super(AutosApi.BASE_AUTOS_URL);
        autosService=createService(AutosService.class);
    }

    //LIST AUTOS
    @Override
    public Call<ArrayList<ClaseAuto>> getAutos(String token) {
        return autosService.getAutos(token);
    }
    //Auto Por id
    @Override
    public Call<ClaseAuto> getAutoId(String id){
        return autosService.getAutoId(id);
    }

    //REGISTER AUTO
    @Override
    public Call<ClaseAuto> autoRegister(MultipartBody.Part imagen,
                                        RequestBody id, RequestBody brand, RequestBody model,
                                        RequestBody year,RequestBody precio) {
        return autosService.autoRegister(imagen,id, brand, model,year,precio);
    }

    //REMOVE AUTO
    @Override
    public Call<ClaseAuto> removeAuto(String id) {
        return autosService.removeAuto(id);
    }

    //STATUS AUTO
    @Override
    public Call<ClaseAuto> statusAuto(String id) {
        return autosService.statusAuto(id);
    }

    //LIST AUTO BY USER
    @Override
    public Call<ArrayList<ClaseAuto>> getAutoByUser(String token, String id) {
        return autosService.getAutoByUser(token, id);
    }

    @Override
    public Call<ModelAlquiler> alquilerRegister(String username, String auto, Date fechareserva, Date fechafin, String totalpagar) {
        return autosService.alquilerRegister(username, auto, fechareserva,fechafin,totalpagar);
    }

    //LIST Alquiler BY USER
    @Override
    public Call<ArrayList<BodyAlquiler>> getAlquilerByUser(String token) {
        return autosService.getAlquilerByUser(token);
    }
}
