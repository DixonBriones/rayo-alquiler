package com.facci.rayoalquiler.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.facci.rayoalquiler.DescripcionActivity;
import com.facci.rayoalquiler.R;
import com.facci.rayoalquiler.models.BodyAlquiler;
import com.facci.rayoalquiler.models.ClaseAuto;
import com.facci.rayoalquiler.models.ModelAlquiler;

import java.util.ArrayList;

public class MisReservasAdapter extends RecyclerView.Adapter<MisReservasAdapter.HolderAlquiler> {

    ArrayList<BodyAlquiler> listaAlquiler;
    Context context;

    public MisReservasAdapter(ArrayList<BodyAlquiler> listaAlquiler, Context context) {
        this.listaAlquiler = listaAlquiler;
        this.context = context;

    }


    @NonNull
    @Override
    public HolderAlquiler onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_mis_reservas, parent, false);
        HolderAlquiler holderAlquiler = new HolderAlquiler(view);
        return holderAlquiler;
    }

    @Override
    public void onBindViewHolder(@NonNull HolderAlquiler holder, int position) {
        BodyAlquiler bodyAlquiler = listaAlquiler.get(position);

        Glide.with(context).load(bodyAlquiler.getFoto()).into(holder.imgAutoAlquiler);
        holder.nameAutoAlquiler.setText(bodyAlquiler.getMarca()+" "+bodyAlquiler.getModelo());
        holder.totalPagarAuto.setText("$ " +bodyAlquiler.getTotalpagar());
        holder.buttonContactar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url= "https://wa.me/+593"+bodyAlquiler.getTelefono();
                Uri uri = Uri.parse(url);
                Intent in = new Intent(Intent.ACTION_VIEW,uri);
                context.startActivity(in);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listaAlquiler.size();
    }

    public class HolderAlquiler extends RecyclerView.ViewHolder {


        ImageView imgAutoAlquiler;
        TextView nameAutoAlquiler, totalPagarAuto;
        ImageButton buttonContactar;


        public HolderAlquiler(@NonNull View itemView) {

            super(itemView);
            imgAutoAlquiler= itemView.findViewById(R.id.imgAutoAlquiler);
            nameAutoAlquiler = itemView.findViewById(R.id.nameAutoAlquiler);
            totalPagarAuto= itemView.findViewById(R.id.totalPagarAuto);
            buttonContactar=itemView.findViewById(R.id.buttonContactar);

        }
    }
}
