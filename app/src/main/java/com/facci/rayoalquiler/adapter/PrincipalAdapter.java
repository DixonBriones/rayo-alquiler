package com.facci.rayoalquiler.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.facci.rayoalquiler.DescripcionActivity;
import com.facci.rayoalquiler.R;
import com.facci.rayoalquiler.models.ClaseAuto;

import java.util.ArrayList;

public class PrincipalAdapter extends RecyclerView.Adapter<PrincipalAdapter.HolderPrincipal>{

    ArrayList<ClaseAuto> listaAutos;
    Context context;

    public PrincipalAdapter(ArrayList<ClaseAuto> listaAutos, Context context) {
        this.listaAutos = listaAutos;
        this.context = context;

    }
    
   // Crear Vista

    @NonNull
    @Override
    public HolderPrincipal onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_auto_principal, parent, false);
        HolderPrincipal holderPrincipal = new HolderPrincipal(view);
        return holderPrincipal;
    }
    //Agregar los datos a la vista
    @Override
    public void onBindViewHolder(@NonNull HolderPrincipal holder, int position) {
        ClaseAuto claseAuto = listaAutos.get(position);

        Glide.with(context).load(claseAuto.getFoto()).into(holder.imgAuto);
        holder.nameAuto.setText(claseAuto.getMarca()+" "+claseAuto.getModelo());
        holder.precioAuto.setText("$ " +claseAuto.getPrecio());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, DescripcionActivity.class);
                intent.putExtra("_id" , claseAuto.getId());
                context.startActivity(intent);
            }
        });
    }



    //Longitud lista
    @Override
    public int getItemCount() {
        return listaAutos.size();
    }

    public class HolderPrincipal extends RecyclerView.ViewHolder {


        ImageView imgAuto;
        TextView nameAuto, precioAuto;


        public HolderPrincipal(@NonNull View itemView) {

            super(itemView);
            imgAuto= itemView.findViewById(R.id.imgAuto);
            nameAuto = itemView.findViewById(R.id.nameAuto);
            precioAuto= itemView.findViewById(R.id.precioAuto);
        }
    }
}
