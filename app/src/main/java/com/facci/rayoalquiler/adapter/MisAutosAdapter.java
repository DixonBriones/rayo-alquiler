package com.facci.rayoalquiler.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.facci.rayoalquiler.AutosFragment;
import com.facci.rayoalquiler.DescripcionActivity;
import com.facci.rayoalquiler.FormRegistroAuto;
import com.facci.rayoalquiler.MainActivity;
import com.facci.rayoalquiler.models.ClaseAuto;
import com.facci.rayoalquiler.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MisAutosAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<ClaseAuto> arrayList;

    public MisAutosAdapter(Context context, ArrayList<ClaseAuto> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public int getCount(){
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        if(convertView == null){
            convertView = View.inflate(context,  R.layout.item_auto_mis_autos, null);
        }

        TextView autoBrand = convertView.findViewById(R.id.txtAutoBrand);
        TextView autoModel = convertView.findViewById(R.id.txtAutoModel);
        ImageView imgAuto = convertView.findViewById(R.id.imgAuto);
        TextView autoId = convertView.findViewById(R.id.txtAutoId);

        //LIST AUTO
        ClaseAuto auto = arrayList.get(position);
        Glide.with(context).load(auto.getFoto()).into(imgAuto);
        autoId.setText(auto.getId());
        autoBrand.setText(auto.getMarca());
        if(auto.getStatus().booleanValue() == false){
            autoModel.setText("NO DISPONIBLE");
        }else {
            autoModel.setText("DISPONIBLE");
        }

        //REMOVE AUTO
        Button btnRemoveAuto = convertView.findViewById(R.id.btnDeleteAuto);
        btnRemoveAuto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnDelete(autoId.getText().toString());

            }
        });

        //STATUS AUTO
        Button btnStatusAuto = convertView.findViewById(R.id.btnStatusAuto);
        btnStatusAuto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnStatus(autoId.getText().toString());
            }
        });

        return convertView;
    }

    public void btnDelete(String id){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Eliminar Auto");
        builder.setMessage("¿Estas seguro que desea eliminar este auto de su galeria?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Intent intent = new Intent(context, PrincipalFragment.class);
                        //context.startActivity(intent);
                        AutosApiAdapter autosApiAdapter = new AutosApiAdapter();
                        Call<ClaseAuto> call = autosApiAdapter.removeAuto(id);
                        call.enqueue(new Callback<ClaseAuto>() {
                            @Override
                            public void onResponse(Call<ClaseAuto> call, Response<ClaseAuto> response) {
                                if (response.isSuccessful()){
                                    Toast.makeText(context, "Auto deleted successfully!", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(context, MainActivity.class);
                                    context.startActivity(intent);
                                }
                                //context.startActivity(new Intent(context, AutosFragment.class));
                            }
                            @Override
                            public void onFailure(Call<ClaseAuto> call, Throwable t) {
                                Log.e("ERROR: ", t.getMessage());
                            }
                        });
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(context, "Cancel", Toast.LENGTH_SHORT).show();
                        dialogInterface.dismiss();
                    }
                }).show();
    }

    public void btnStatus(String id){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Cambiar estado");
        builder.setMessage("¿Estas seguro que desea cambiar el estado del auto?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        AutosApiAdapter autosApiAdapter = new AutosApiAdapter();
                        Call<ClaseAuto> call = autosApiAdapter.statusAuto(id);
                        call.enqueue(new Callback<ClaseAuto>() {
                            @Override
                            public void onResponse(Call<ClaseAuto> call, Response<ClaseAuto> response) {
                                if (response.isSuccessful()){
                                    Toast.makeText(context, "Status change successfully!", Toast.LENGTH_SHORT).show();
                                }
                                //context.startActivity(new Intent(context, AutosFragment.class));
                            }
                            @Override
                            public void onFailure(Call<ClaseAuto> call, Throwable t) {
                                Log.e("ERROR: ", t.getMessage());
                            }
                        });
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(context, "Cancel", Toast.LENGTH_SHORT).show();
                        dialogInterface.dismiss();
                    }
                }).show();
    }
}