package com.facci.rayoalquiler.adapter;

import com.facci.rayoalquiler.contans.AutosApi;
import com.facci.rayoalquiler.models.ModelUser;
import com.facci.rayoalquiler.models.ModelUserLogin;
import com.facci.rayoalquiler.services.UserService;

import retrofit2.Call;

public class AdapterUser
    extends BaseAdapter
    implements UserService {

    private UserService serviceUser;

    public AdapterUser() {
        super(AutosApi.BASE_AUTOS_URL);
        serviceUser = createService(UserService.class);
    }

    @Override
    public Call<ModelUser> userRegister(String username, String password, String telephone) {
        return serviceUser.userRegister(username, password, telephone);
    }

    @Override
    public Call<ModelUser> userLogin(ModelUserLogin modelUserLogin) {
        return serviceUser.userLogin(modelUserLogin);
    }

    @Override
    public Call<ModelUser> userUpdate(String id, ModelUser modelUser) {
        return serviceUser.userUpdate(id, modelUser);
    }

    @Override
    public Call<ModelUser> removeUser(String id) {
        return serviceUser.removeUser(id);
    }

}