package com.facci.rayoalquiler;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.facci.rayoalquiler.adapter.AutosApiAdapter;
import com.facci.rayoalquiler.contans.Auth;
import com.facci.rayoalquiler.models.ClaseAuto;
import com.facci.rayoalquiler.models.ModelAlquiler;

import org.imaginativeworld.whynotimagecarousel.ImageCarousel;
import org.imaginativeworld.whynotimagecarousel.model.CarouselItem;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DescripcionActivity extends AppCompatActivity {
    DatePickerDialog datePickerDialog, datePickerDialog2;
    Button buttonRecepcion, buttonDevolucion, buttonEnviar;
    TextView textViewPropietario,textViewPrecio,textViewMarca,textViewModelo,textViewYear,textViewTransmision,textViewTitulo;
    String telefono;
    Long precio;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_descripcion);
        String id = getIntent().getStringExtra("_id");

        //OPTION BACK
        //ActionBar actionBar = getSupportActionBar();
        //actionBar.setDisplayHomeAsUpEnabled(true);



        buttonEnviar = findViewById(R.id.button);
        buttonEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showConfirmation();
            }
        });


        //DatePicker
        initDatePicker();
        initDatePicker2();
        buttonRecepcion= findViewById(R.id.datePickerButtonRecoleccion);
        buttonRecepcion.setText(getTodayDate());
        buttonDevolucion= findViewById(R.id.datePickerButtonDevolucion);
        buttonDevolucion.setText(getTodayDate());


        /*

        //Leer Json
        String json = "{\"propietario\":\"Tony Stark\",\"precio\":\"25.00\",\"marca\":\"Suzuki\",\"modelo\":\"XYZ\",\"year\":\"2021\",\"transmision\":\"Manual\"}";
        Moshi moshi = new Moshi.Builder().build();
        JsonAdapter<ClaseAuto> jsonAdapter = moshi.adapter(ClaseAuto.class);

        ClaseAuto claseAuto = null;
        try {
            claseAuto = jsonAdapter.fromJson(json);
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        textViewTitulo=findViewById(R.id.textViewTitulo);
        textViewPropietario= findViewById(R.id.textViewPropietario);
        textViewPrecio= findViewById(R.id.textViewPrecio);
        textViewMarca= findViewById(R.id.textViewMarca);
        textViewModelo= findViewById(R.id.textViewModelo);
        textViewYear= findViewById(R.id.textViewYear);
        textViewTransmision= findViewById(R.id.textViewTransmision);


        //Carrusel
        ImageCarousel carousel = findViewById(R.id.carouselDescripcion);
        carousel.registerLifecycle(getLifecycle());

        mostrarDetalles(id,carousel);

    }

    private void mostrarDetalles(String id,ImageCarousel carousel ){
        AutosApiAdapter autosApiAdapter = new AutosApiAdapter();

        Call<ClaseAuto> call = autosApiAdapter.getAutoId( id );

        call.enqueue(new Callback<ClaseAuto>() {
            @Override
            public void onResponse(Call<ClaseAuto> call, Response<ClaseAuto> response) {
                ClaseAuto claseAuto = response.body();
                String titulo= claseAuto.getMarca()+ " " + claseAuto.getModelo();
                textViewTitulo.setText(titulo);
                textViewPropietario.setText(claseAuto.getPropietario());
                textViewPrecio.setText("$"+ claseAuto.getPrecio());
                precio= Long.parseLong(claseAuto.getPrecio());
                textViewMarca.setText(claseAuto.getMarca());
                textViewModelo.setText(claseAuto.getModelo());
                textViewYear.setText(claseAuto.getYear());
                textViewTransmision.setText(claseAuto.getTransmision());
                telefono= claseAuto.getTelefono();

                List<CarouselItem> list = new ArrayList<>();
                list.add(
                        new CarouselItem(
                                claseAuto.getFoto()
                        )
                );
                carousel.setData(list);

            }

            @Override
            public void onFailure(Call<ClaseAuto> call, Throwable t) {

            }
        });

    }

    private String getTodayDate() {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        month = month +1;
        int day= cal.get(Calendar.DAY_OF_MONTH);
        return makeDateString(day,month,year);
    }

    private void initDatePicker() {
        DatePickerDialog.OnDateSetListener dateSetListener= new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month +1;
                String date= makeDateString(day,month,year);
                Log.e("fecha",date);
                buttonRecepcion.setText(date);
            }
        };
        Calendar cal= Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day= cal.get(Calendar.DAY_OF_MONTH);


        datePickerDialog= new DatePickerDialog(this, dateSetListener, year,month,day);
        Date dateStart = obtenerFechas(datePickerDialog.getDatePicker());
        datePickerDialog.getDatePicker().setMinDate(dateStart.getTime() );
    }

    private void initDatePicker2() {
        DatePickerDialog.OnDateSetListener dateSetListener= new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month +1;
                String date= makeDateString(day,month,year);
                Log.e("fecha",date);
                buttonDevolucion.setText(date);
            }
        };
        Calendar cal= Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day= cal.get(Calendar.DAY_OF_MONTH);
        //datePickerDialog2.getDatePicker().setMinDate(cal.getTimeInMillis());
        datePickerDialog2= new DatePickerDialog(this, dateSetListener, year,month,day+1);

    }

    private String makeDateString(int day, int month, int year) {
        return day+"/" +month+"/"+year;
    }


    public void openDatePicker(View view) {
        datePickerDialog.show();

    }
    public void openDatePicker2(View view) {
        Date dateStart = obtenerFechas(datePickerDialog.getDatePicker());
        datePickerDialog2.getDatePicker().setMinDate(dateStart.getTime()+86400000);
        datePickerDialog2.show();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public static java.util.Date obtenerFechas(DatePicker fechadatepicker){
        int day = fechadatepicker.getDayOfMonth();
        int month = fechadatepicker.getMonth();
        int year =  fechadatepicker.getYear();

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);

        return calendar.getTime();
    }

    public void showConfirmation(){
        Date dateStart = obtenerFechas(datePickerDialog.getDatePicker());
        Date dateEnd = obtenerFechas(datePickerDialog2.getDatePicker());

        long difference = Math.abs(dateEnd.getTime() - dateStart.getTime());
        difference= difference / (24*60 * 60 * 1000);
        Long preciofinal =precio * difference;


        AlertDialog.Builder builder = new AlertDialog.Builder(DescripcionActivity.this);
        builder.setTitle("Confirmar reserva");
        builder.setMessage("¿Desea confirmar la reserva? El total seria de: $"+preciofinal )
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {

                        String idauto = getIntent().getStringExtra("_id");
                        registerAlquiler(Auth._ID,idauto,
                                obtenerFechas(datePickerDialog.getDatePicker()),
                                obtenerFechas(datePickerDialog2.getDatePicker()),
                                preciofinal.toString());
                        /*
                        String url= "https://wa.me/+593"+telefono;
                        Uri uri = Uri.parse(url);
                        Intent in = new Intent(Intent.ACTION_VIEW,uri);
                        startActivity(in);
                        */
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(getApplicationContext(), "Cancel", Toast.LENGTH_SHORT).show();
                        dialogInterface.dismiss();
                    }
                }).show();
    }
    public void registerAlquiler(String idUsuario,String idAuto,Date fecha1,Date fecha2,String precios){
        Log.e("ds",fecha1.toString());

        AutosApiAdapter autosApiAdapter = new AutosApiAdapter();
        autosApiAdapter.alquilerRegister(idUsuario, idAuto, fecha1, fecha2, precios).enqueue(new Callback<ModelAlquiler>() {
            @Override
            public void onResponse(Call<ModelAlquiler> call, Response<ModelAlquiler> response) {
                Log.e("MSG", "Guardado ");
                Toast.makeText(getApplicationContext(), "Reserva realizada", Toast.LENGTH_SHORT).show();
                Intent in = new Intent(DescripcionActivity.this,MainActivity.class);
                startActivity(in);
            }

            @Override
            public void onFailure(Call<ModelAlquiler> call, Throwable t) {

            }
        });

    }
}
