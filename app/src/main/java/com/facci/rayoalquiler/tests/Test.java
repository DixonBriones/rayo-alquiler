package com.facci.rayoalquiler.tests;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Test {
    public boolean validEmpty(String user, String pass){
        if (user.isEmpty() && pass.isEmpty()){
            return true;
        }
        return false;
    }
    public boolean validNoEmpty(String user, String pass){
        if (user.isEmpty() && pass.isEmpty()){
            return false;
        }
        return true;
    }
    public boolean validEmail(String email){
        String exp = "/^\\w+@\\w+\\.+[a-z]{3}$/";
        Pattern pattern = Pattern.compile(exp);
        Matcher matcher = pattern.matcher(email);
        if (matcher.matches())
            return false;
        return true;
    }
}
