package com.facci.rayoalquiler;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.facci.rayoalquiler.adapter.AdapterUser;
import com.facci.rayoalquiler.adapter.AutosApiAdapter;
import com.facci.rayoalquiler.contans.Auth;
import com.facci.rayoalquiler.models.ClaseAuto;
import com.facci.rayoalquiler.models.ModelUser;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PerfilFragment extends Fragment {

    public PerfilFragment() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_perfil, container, false);

        //UPDATE ACCOUNT USER
        FloatingActionButton btnAlquiler = v.findViewById(R.id.fabMisReservas);
        btnAlquiler.setOnClickListener(view -> {
            Intent i = new Intent(getActivity(), MisReservasActivity.class);
            startActivity(i);
        });

        //UPDATE ACCOUNT USER
        FloatingActionButton btnEditUser = v.findViewById(R.id.fabUpdateUser);
        btnEditUser.setOnClickListener(view -> {
            Intent i = new Intent(getActivity(), ActivityEditarPerfil.class);
            startActivity(i);
        });

        //REMOVE ACCOUNT USER
        FloatingActionButton btnRemoveUser = v.findViewById(R.id.fabRemoveUser);
        btnRemoveUser.setOnClickListener(view -> {
            btnDelete(Auth._ID);
        });

        //CLOSE ACCOUNT USER
        FloatingActionButton btnClose = v.findViewById(R.id.fabSignOff);
        btnClose.setOnClickListener(view -> {
            btnClose();
        });

        TextView textView = v.findViewById(R.id.txtUsernameProfile);
        textView.setText(Auth._USERNAME);

        TextView textView2 = v.findViewById(R.id.txtNumberPhone);
        textView2.setText(Auth._TELEPHONE);

        TextView textView3 = v.findViewById(R.id.txtLocationProfile);
        textView3.setText(Auth._LOCATION);

        TextView textView4 = v.findViewById(R.id.txtEmailProfile);
        textView4.setText(Auth._USEREMAIL);

        return v;
    }

    public void btnDelete(String id){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Remove account..!");
        builder.setMessage("¿Estas seguro que deseas eliminar tu cuenta?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        AdapterUser adapterUser = new AdapterUser();
                        Call<ModelUser> call = adapterUser.removeUser(id);
                        call.enqueue(new Callback<ModelUser>() {
                            @Override
                            public void onResponse(Call<ModelUser> call, Response<ModelUser> response) {
                                if (response.isSuccessful()){
                                    Toast.makeText(getActivity(), "Account deleted successfully!", Toast.LENGTH_SHORT).show();
                                    getActivity().startActivity(new Intent(getActivity(), AuthActivity.class));
                                }
                            }
                            @Override
                            public void onFailure(Call<ModelUser> call, Throwable t) {
                                Log.e("ERROR: ", t.getMessage());
                            }
                        });
                        startActivity(new Intent(getActivity(), AuthActivity.class));
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(getActivity(), "Cancel", Toast.LENGTH_SHORT).show();
                        dialogInterface.dismiss();
                    }
                }).show();
    }

    public void btnClose(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Sign off..!");
        builder.setMessage("¿Estas seguro que deseas cerrar sesion?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        System.exit(0);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(getActivity(), "Cancel", Toast.LENGTH_SHORT).show();
                        dialogInterface.dismiss();
                    }
                }).show();
    }
}