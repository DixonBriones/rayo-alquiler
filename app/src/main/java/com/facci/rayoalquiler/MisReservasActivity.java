package com.facci.rayoalquiler;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.facci.rayoalquiler.adapter.AutosApiAdapter;
import com.facci.rayoalquiler.adapter.MisReservasAdapter;
import com.facci.rayoalquiler.adapter.PrincipalAdapter;
import com.facci.rayoalquiler.contans.Auth;
import com.facci.rayoalquiler.models.BodyAlquiler;
import com.facci.rayoalquiler.models.ClaseAuto;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MisReservasActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    MisReservasAdapter misReservasAdapter;
    ArrayList<BodyAlquiler> listaAlquiler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mis_reservas);
        mostrarDatos();
    }

    private void mostrarDatos() {
        AutosApiAdapter autosApiAdapter = new AutosApiAdapter();

        Call<ArrayList<BodyAlquiler>> call = autosApiAdapter.getAlquilerByUser(Auth.TOKEN);
        call.enqueue(new Callback<ArrayList<BodyAlquiler>>() {
            @Override
            public void onResponse(Call<ArrayList<BodyAlquiler>> call, Response<ArrayList<BodyAlquiler>> response) {
                listaAlquiler= new ArrayList<>();
                listaAlquiler = response.body();
                recyclerView= findViewById(R.id.rcvReservas);
                misReservasAdapter = new MisReservasAdapter(listaAlquiler, MisReservasActivity.this);
                recyclerView.setAdapter(misReservasAdapter);
                recyclerView.setLayoutManager(new LinearLayoutManager(MisReservasActivity.this , RecyclerView.VERTICAL,false));
                misReservasAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<ArrayList<BodyAlquiler>> call, Throwable t) {
                Log.e("Error", "Se encontro un error");
            }
        });

    }
}