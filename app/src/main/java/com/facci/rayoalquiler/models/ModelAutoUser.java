package com.facci.rayoalquiler.models;

import java.util.List;

public class ModelAutoUser {

    private boolean status;
    private List<ClaseAuto> autos;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<ClaseAuto> getAutos() {
        return autos;
    }

    public void setAutos(List<ClaseAuto> autos) {
        this.autos = autos;
    }
}
