package com.facci.rayoalquiler.models;

import com.google.gson.annotations.SerializedName;

public class ModelUser {

    @SerializedName("_id")
    private String _id;

    @SerializedName("telefono")
    private String telephone;

    @SerializedName("username")
    private String username;

    @SerializedName("password")
    private String password;

    @SerializedName("useremail")
    private String useremail;

    @SerializedName("firstname")
    private String firstname;

    @SerializedName("lastname")
    private String lastname;

    @SerializedName("location")
    private String location;

    @SerializedName("accessToken")
    private String accessToken;

    public ModelUser() {
    }

    public ModelUser(String telephone, String useremail, String firstname, String lastname, String location) {
        this.telephone = telephone;
        this.useremail = useremail;
        this.firstname = firstname;
        this.lastname = lastname;
        this.location = location;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getUseremail() {
        return useremail;
    }

    public void setUseremail(String useremail) {
        this.useremail = useremail;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
