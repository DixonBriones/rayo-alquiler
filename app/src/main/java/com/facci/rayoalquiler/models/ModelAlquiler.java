package com.facci.rayoalquiler.models;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class ModelAlquiler {
    @SerializedName("usuario")
    String usuario;
    @SerializedName("auto")
    String auto;
    @SerializedName("fechareserva")
    Date fechareserva;
    @SerializedName("fechafinreserva")
    Date fechafinreserva;
    @SerializedName("totalpagar")
    String totalpagar;

    public ModelAlquiler() {
    }

    public ModelAlquiler(String usuario, String auto, Date fechareserva, Date fechafinreserva, String totalpagar) {
        this.usuario = usuario;
        this.auto = auto;
        this.fechareserva = fechareserva;
        this.fechafinreserva = fechafinreserva;
        this.totalpagar = totalpagar;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getAuto() {
        return auto;
    }

    public void setAuto(String auto) {
        this.auto = auto;
    }

    public Date getFechareserva() {
        return fechareserva;
    }

    public void setFechareserva(Date fechareserva) {
        this.fechareserva = fechareserva;
    }

    public Date getFechafinreserva() {
        return fechafinreserva;
    }

    public void setFechafinreserva(Date fechafinreserva) {
        this.fechafinreserva = fechafinreserva;
    }

    public String getTotalpagar() {
        return totalpagar;
    }

    public void setTotalpagar(String totalpagar) {
        this.totalpagar = totalpagar;
    }
}
