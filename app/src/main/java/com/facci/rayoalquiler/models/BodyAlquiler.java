package com.facci.rayoalquiler.models;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class BodyAlquiler {
    @SerializedName("_id")
    String _id;
    @SerializedName("fechareserva")
    Date fechareserva;
    @SerializedName("fechafinreserva")
    Date fechafinreserva;
    @SerializedName("totalpagar")
    String totalpagar;
    @SerializedName("telefono")
    String telefono;
    @SerializedName("marca")
    String marca;
    @SerializedName("modelo")
    String modelo;
    @SerializedName("foto")
    String foto;

    public BodyAlquiler() {
    }

    public BodyAlquiler(String _id, Date fechareserva, Date fechafinreserva, String totalpagar, String telefono, String marca, String modelo, String foto) {
        this._id = _id;
        this.fechareserva = fechareserva;
        this.fechafinreserva = fechafinreserva;
        this.totalpagar = totalpagar;
        this.telefono = telefono;
        this.marca = marca;
        this.modelo = modelo;
        this.foto = foto;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public Date getFechareserva() {
        return fechareserva;
    }

    public void setFechareserva(Date fechareserva) {
        this.fechareserva = fechareserva;
    }

    public Date getFechafinreserva() {
        return fechafinreserva;
    }

    public void setFechafinreserva(Date fechafinreserva) {
        this.fechafinreserva = fechafinreserva;
    }

    public String getTotalpagar() {
        return totalpagar;
    }

    public void setTotalpagar(String totalpagar) {
        this.totalpagar = totalpagar;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}
