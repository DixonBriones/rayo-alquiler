package com.facci.rayoalquiler.models;

import com.google.gson.annotations.SerializedName;

public class ClaseAuto {
    @SerializedName("_id")
    String _id;
    @SerializedName("usuario")
    String propietario;
    @SerializedName("telefono")
    String telefono;
    @SerializedName("precio")
    String precio;
    @SerializedName("marca")
    String marca;
    @SerializedName("modelo")
    String modelo;
    @SerializedName("foto")
    String foto;
    @SerializedName("year")
    String year;
    @SerializedName("traccion")
    String transmision;

    @SerializedName("status")
    Boolean status;

    public ClaseAuto() {
    }

    public ClaseAuto( String _id, String propietario, String precio, String marca, String modelo, String foto, String year, String transmision, Boolean status) {
        this._id= _id;
        this.propietario = propietario;
        this.precio = precio;
        this.marca = marca;
        this.modelo = modelo;
        this.foto = foto;
        this.year = year;
        this.transmision = transmision;
        this.status = status;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public String getPropietario() {
        return propietario;
    }

    public void setPropietario(String propietario) {
        this.propietario = propietario;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getTransmision() {
        return transmision;
    }

    public void setTransmision(String transmision) {
        this.transmision = transmision;
    }
}
