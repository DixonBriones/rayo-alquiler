package com.facci.rayoalquiler.contans;

public class AutosApi {
    // BASE URL
    public static final String BASE_AUTOS_URL = "https://apirayo.herokuapp.com/";
    //public static final String BASE_AUTOS_URL = "http://10.0.2.2:3000/";


    //ENDPOINTS AUTOS ********
        // LIST AND REGISTER AUTOS
    public static final String AUTOS_ENDPOINT = "api/v1/auto";
        //Autos ID
    public static final String AUTOSID_ENDPOINT="api/v1/auto/findById/{id}";
        // LIST AUTO BY USER
    public static final String AUTOS_ENDPOINT_BY_USER = "api/v1/auto/{id}";
        // DELETE AUTO
    public static final String AUTO_DELETE = "api/v1/auto/{id}";
        // STATUS AUTO
    public static final String AUTO_STATUS = "api/v1/auto/status/{id}";
    // Insert alquiler
    public static final String ALQUILER_ENDPOINT = "api/v1/alquiler/insertaralquiler";
    // List alquiler
    public static final String ALQUILERID_ENDPOINT = "api/v1/alquiler/findalquilerid";

    // ENDPOINTS USER ********
    public static final String REGISTER_ENDPOINT = "api/v1/user/register";
    public static final String LOGIN_ENDPOINT = "api/v1/user/login";
    public static final String REMOVE_ENDPOINT = "api/v1/user/{id}";
    public static final String UPDATE_ENDPOINT = "api/v1/user/{id}";
}
