package com.facci.rayoalquiler.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.facci.rayoalquiler.contans.AutosApi;
import com.facci.rayoalquiler.models.BodyAlquiler;
import com.facci.rayoalquiler.models.ClaseAuto;
import com.facci.rayoalquiler.models.ModelAlquiler;
import com.facci.rayoalquiler.models.ModelAutoUser;
import com.facci.rayoalquiler.models.ModelUser;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface AutosService {

    //LIST Alquiler
    @GET(AutosApi.ALQUILERID_ENDPOINT)
    Call<ArrayList<BodyAlquiler>> getAlquilerByUser(
            @Header("x-access-token") String token);

    //REGISTER Alquiler
    @POST(AutosApi.ALQUILER_ENDPOINT)
    @FormUrlEncoded
    Call<ModelAlquiler> alquilerRegister(@Field("usuario") String username,
                                         @Field("auto") String autoid,
                                         @Field("fechareserva") Date fechareserva,
                                         @Field("fechafinreserva") Date fechafin,
                                         @Field("totalpagar") String totalpagar);

    //LIST AUTOS
    @GET(AutosApi.AUTOS_ENDPOINT)
    Call<ArrayList<ClaseAuto>> getAutos(
                                @Header("x-access-token") String token);
    //Autos ID
    @GET(AutosApi.AUTOSID_ENDPOINT)
    Call<ClaseAuto> getAutoId(
            @Path("id")String id
    );

    //LIST AUTOS BY USER
    @GET(AutosApi.AUTOS_ENDPOINT_BY_USER)
    Call<ArrayList<ClaseAuto>> getAutoByUser(
            @Header("x-access-token") String token,
            @Path("id") String id);

    //ADD AUTO IMG
    @Multipart
    @POST(AutosApi.AUTOS_ENDPOINT)
    Call<ClaseAuto> autoRegister(@Part MultipartBody.Part imagen,
                                 @Part("usuario") RequestBody usuario,
                                 @Part("marca") RequestBody marca,
                                 @Part("modelo") RequestBody modelo,
                                 @Part("year") RequestBody year,
                                 @Part("precio") RequestBody precio);

    /*
    //ADD AUTO
    @POST(AutosApi.AUTOS_ENDPOINT)
    @FormUrlEncoded
    Call<ClaseAuto> autoRegister(@Field("usuario") String id,
            @Field("marca") String marca,
            @Field("modelo") String modelo);
    */
    //REMOVE AUTO
    @DELETE(AutosApi.AUTO_DELETE)
    Call<ClaseAuto> removeAuto(@Path("id") String id);

    //STATUS AUTO
    @GET(AutosApi.AUTO_STATUS)
    Call<ClaseAuto> statusAuto(@Path("id") String id);
}
