package com.facci.rayoalquiler.services;

import com.facci.rayoalquiler.contans.Auth;
import com.facci.rayoalquiler.contans.AutosApi;
import com.facci.rayoalquiler.models.ClaseAuto;
import com.facci.rayoalquiler.models.ModelUser;
import com.facci.rayoalquiler.models.ModelUserLogin;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface UserService {


    //REGISTER ******
    @POST(AutosApi.REGISTER_ENDPOINT)
    @FormUrlEncoded
    Call<ModelUser> userRegister(@Field("telefono") String telephone,
                                 @Field("username") String username,
                                 @Field("password") String password);

    //LOGIN *********
    @POST(AutosApi.LOGIN_ENDPOINT)
    Call<ModelUser> userLogin(@Body ModelUserLogin modelUserLogin);

    //UPDATE ********
    @PUT(AutosApi.UPDATE_ENDPOINT)
    Call<ModelUser> userUpdate(@Path("id") String id,
                               @Body ModelUser modelUser);

    //REMOVE ********
    @DELETE(AutosApi.REMOVE_ENDPOINT)
    Call<ModelUser> removeUser(@Path("id") String id);

}
