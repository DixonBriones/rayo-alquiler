package com.facci.rayoalquiler;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.facci.rayoalquiler.adapter.AdapterUser;
import com.facci.rayoalquiler.contans.Auth;
import com.facci.rayoalquiler.models.ModelUser;
import com.facci.rayoalquiler.models.ModelUserLogin;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialog;
import com.rengwuxian.materialedittext.MaterialEditText;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AuthActivity extends AppCompatActivity {

    EditText edtUsername, edtPassword;
    TextView txtRegister;
    Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        edtUsername = findViewById(R.id.edtUsername);
        edtPassword = findViewById(R.id.edtPassword);

        btnLogin = findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(TextUtils.isEmpty(edtUsername.getText().toString())){
                    Toast.makeText(AuthActivity.this, "Enter a username", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(TextUtils.isEmpty(edtPassword.getText().toString())){
                    Toast.makeText(AuthActivity.this, "Enter a password", Toast.LENGTH_SHORT).show();
                    return;
                }
                loginUser(edtUsername.getText().toString(), edtPassword.getText().toString());
            }
        });

        txtRegister = findViewById(R.id.txtRegister);
        txtRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View register_layout = LayoutInflater.from(AuthActivity.this)
                        .inflate(R.layout.register_layout, null);
                new MaterialStyledDialog.Builder(AuthActivity.this)
                        .setIcon(R.drawable.logo)
                        .setTitle("Sign Up")
                        .setCustomView(register_layout)

                        .setNegativeText("CANCEL")
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        })

                        .setPositiveText("REGISTER")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                MaterialEditText telephone = register_layout.findViewById(R.id.edtTelephone);
                                MaterialEditText username = register_layout.findViewById(R.id.edtUsername);
                                MaterialEditText password = register_layout.findViewById(R.id.edtPassword);

                                if(TextUtils.isEmpty(telephone.getText().toString())){
                                    Toast.makeText(AuthActivity.this, "Enter a telephone", Toast.LENGTH_SHORT).show();
                                    return;
                                }
                                if(TextUtils.isEmpty(username.getText().toString())){
                                    Toast.makeText(AuthActivity.this, "Enter a username", Toast.LENGTH_SHORT).show();
                                    return;
                                }
                                if(TextUtils.isEmpty(password.getText().toString())){
                                    Toast.makeText(AuthActivity.this, "Enter a password", Toast.LENGTH_SHORT).show();
                                    return;
                                }

                                registerUser(telephone.getText().toString().trim(), username.getText().toString().trim(), password.getText().toString().trim());
                            }
                        })
                        .show();
            }
        });
    }

    public void registerUser(String telephone, String username, String password){
        AdapterUser adapterUser = new AdapterUser();
        adapterUser.userRegister(telephone, username, password)
                .enqueue(new Callback<ModelUser>() {
                    @Override
                    public void onResponse(Call<ModelUser> call, Response<ModelUser> response) {
                        if(response.isSuccessful()){
                            ModelUser modelUser = response.body();
                            //Log.e("Success", response.body().toString());
                            Toast.makeText(AuthActivity.this, "Registered user successfully", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ModelUser> call, Throwable t) {
                        Log.e("Error", "xd of error");
                        Toast.makeText(AuthActivity.this, "Fail app", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void loginUser(String username, String password){
        AdapterUser adapterUser = new AdapterUser();
        adapterUser.userLogin(new ModelUserLogin(username, password))
                .enqueue(new Callback<ModelUser>() {
                    @Override
                    public void onResponse(Call<ModelUser> call, Response<ModelUser> response) {
                        ModelUser modelUser = response.body();
                        Auth.TOKEN = modelUser.getAccessToken();
                        Auth._ID = modelUser.get_id();
                        Auth._TELEPHONE = modelUser.getTelephone();
                        Auth._USERNAME = modelUser.getUsername();
                        Auth._USEREMAIL = modelUser.getUseremail();
                        Auth._FIRSTNAME = modelUser.getFirstname();
                        Auth._LASTNAME = modelUser.getLastname();
                        Auth._LOCATION = modelUser.getLocation();
                        try {
                            if(modelUser.getAccessToken().isEmpty()){
                                Toast.makeText(AuthActivity.this, "Not authorized", Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(AuthActivity.this, "Bienvenido", Toast.LENGTH_SHORT).show();
                                if(response.isSuccessful()) {
                                    startActivity(new Intent(AuthActivity.this, MainActivity.class).putExtra("token", response.body().getAccessToken()).putExtra("username", response.body().getUsername()));
                                }
                            }
                        }catch (Exception e){
                            Toast.makeText(AuthActivity.this, "Ups, User not found", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ModelUser> call, Throwable t) {
                        Toast.makeText(AuthActivity.this, "Connection Fail", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}