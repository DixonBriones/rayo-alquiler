package com.facci.rayoalquiler;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.facci.rayoalquiler.adapter.AutosApiAdapter;
import com.facci.rayoalquiler.contans.Auth;
import com.facci.rayoalquiler.models.ClaseAuto;
import com.facci.rayoalquiler.utils.RealPathUtil;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FormRegistroAuto extends AppCompatActivity {

    EditText edtBrand, edtModel,edtYear,edtPrice;
    Button btnRegisterAuto;
    ImageView imgImage;
    String path;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_registro_auto);

        edtBrand = findViewById(R.id.edtAutoBrand);
        edtModel = findViewById(R.id.edtAutoModel);
        edtYear= findViewById(R.id.edtAutoYear);
        edtPrice= findViewById(R.id.edtAutoPrice);
        imgImage = findViewById(R.id.imgAutoImage);

        FloatingActionButton btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(view -> {
            Intent i = new Intent(FormRegistroAuto.this, MainActivity.class);
            startActivity(i);
        });

        //Log.e("ID", Auth._ID);

        btnRegisterAuto = findViewById(R.id.btnRegisterAuto);
        btnRegisterAuto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //showConfirmation();
                registerAuto(Auth._ID, edtBrand.getText().toString(),
                        edtModel.getText().toString(), edtYear.getText().toString(),edtPrice.getText().toString());
                //edtBrand.setText("");
                //edtModel.setText("");
            }
        });

    }

    public void registerAuto(String idb, String brandb, String modelb,String yearb, String preciob) {
        AlertDialog.Builder builder = new AlertDialog.Builder(FormRegistroAuto.this);
        builder.setTitle("Alquilar Auto");
        builder.setMessage("¿Estas seguro que deseas alquilar este auto?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        AutosApiAdapter autosApiAdapter = new AutosApiAdapter();
                        File file= new File(path);
                        RequestBody requestFile= RequestBody.create(MediaType.parse("multipart/form-data"),file);
                        MultipartBody.Part body= MultipartBody.Part.createFormData("imagen",file.getName(),requestFile);
                        RequestBody id = RequestBody.create(MediaType.parse("multipart/form-data"),idb);
                        RequestBody brand = RequestBody.create(MediaType.parse("multipart/form-data"),brandb);
                        RequestBody model = RequestBody.create(MediaType.parse("multipart/form-data"),modelb);
                        RequestBody year = RequestBody.create(MediaType.parse("multipart/form-data"),yearb);
                        RequestBody price = RequestBody.create(MediaType.parse("multipart/form-data"),preciob);
                        Log.e("dsdsd",body.toString());
                        autosApiAdapter.autoRegister(body,id,brand,model,year,price)
                                .enqueue(new Callback<ClaseAuto>() {
                                    @Override
                                    public void onResponse(Call<ClaseAuto> call, Response<ClaseAuto> response) {
                                        if (response.isSuccessful()) {
                                            //ClaseAuto claseAuto = response.body();
                                            Toast.makeText(FormRegistroAuto.this, "Auto add successfully!", Toast.LENGTH_SHORT).show();
                                            startActivity(new Intent(FormRegistroAuto.this, MainActivity.class));
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<ClaseAuto> call, Throwable t) {
                                        Log.e("Error", t.toString());
                                        Toast.makeText(FormRegistroAuto.this, "Fail app", Toast.LENGTH_SHORT).show();
                                    }
                                });
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Toast.makeText(getApplicationContext(), "Cancel", Toast.LENGTH_SHORT).show();
                        dialogInterface.dismiss();
                    }
                }).show();


    }

    public void onclick(View view) {
        uploadImage();
    }

    private void uploadImage() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/");
        startActivityForResult(intent.createChooser(intent, "Select a app"), 10);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK){
            Uri uri = data.getData();
            imgImage.setImageURI(uri);
            path= RealPathUtil.getRealPath(this,uri);
        }
    }
}